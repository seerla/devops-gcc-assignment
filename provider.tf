terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.57.1"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
  default_tags {
    tags = {
      Terraform   = "true"
      Environment = "govtech-gcc-assignment"
    }
  }
}