data "aws_region" "currentregion" {}

# We have manually put rest api content inside resrapioutput.json for building CIDR
data "external" "restapidata" {
  program = ["cat", var.external_data_restpai_output_file]
}



/*

data "aws_ami" "govtech_gcc_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

/*
data "aws_iam_policy_document" "allow_access" {
  statement {
    "Effect": "Allow",
    "Principal": "*",
    "Action": "*",
    "Resource": "arn:aws:s3:::govtech-gcc-bucket/*"
  }
}

*/

/**
# This section can used in Real REST API url endpoint
data "http" "vend_ip" {
  url = "https://FDQN/vend_ip"
}

variable "ip_address" {
  description = "IP address retrieved from the REST API"
  type        = string
}

variable "subnet_size" {
  description = "Subnet size retrieved from the REST API"
  type        = string
}

# Assign values from the data source to variables
locals {
  ip_address   = data.http.vend_ip.body.ip_address
  subnet_size  = data.http.vend_ip.body.subnet_size
}

*/