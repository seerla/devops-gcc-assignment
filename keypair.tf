resource "tls_private_key" "ec2keypair" {
  algorithm = "RSA"
  rsa_bits  = 2048
}



resource "aws_key_pair" "deployer" {
  key_name   = "ec2gcckeypair"
  public_key = tls_private_key.ec2keypair.public_key_openssh
}
