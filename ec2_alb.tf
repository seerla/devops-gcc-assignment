resource "aws_lb" "govtech_gcc_alb" {
  name               = "govtech-gcc-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.http_sg_external.id]
  subnets            = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
}

resource "aws_lb_target_group" "govtech_gcc_tg" {
  name        = "govtech-gcc-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.govtrch_gcc_vpc.id
}

resource "aws_lb_listener" "govtech_gcc_listener" {
  load_balancer_arn = aws_lb.govtech_gcc_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.govtech_gcc_tg.arn
    type             = "forward"
  }
}


resource "aws_instance" "govtech_gcc_instance" {
  for_each = {
    "gcc-instance1" = local.subnet1_id
    "gcc-instance2" = local.subnet2_id
  }
  ami           = var.ami
  instance_type          = "t2.micro"
  key_name               = "ec2gcckeypair"
  vpc_security_group_ids = [aws_security_group.http_sg_internal.id]
  subnet_id              = each.value
  monitoring             = true

  tags = {
    Name = "govtech-gcc-ec2-${each.key}"
  }

  user_data = <<-EOF
    #!/bin/bash
    sudo apt update
    sudo apt install -y nginx
    sudo systemctl enable nginx
    sudo systemctl start nginx
  EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "gcc_targetgroup_attachment" {
  target_group_arn = aws_lb_target_group.govtech_gcc_tg.arn

  for_each = aws_instance.govtech_gcc_instance

  target_id = each.value.id
}