variable "external_data_restpai_output_file" {
  description = "Path to a file containing JSON data with 'ip_address' and 'subnet_size'"
  type        = string
  default     = "./resrapioutput.json"
}

variable "az" {
  type    = list(string)
  default = ["ap-southeast-1a", "ap-southeast-1b"]
}


variable "ami" {
  description = "The ID of the Amazon Machine Image (AMI)"
  type        = string
  default     = "ami-0fa377108253bf620"
}