# devops-gcc-assignment


## Assumptions and Execution

1. REST API URL output stored in resrapioutput.json
2. Terraform used as Infrastructure as Code (IaC)
2. Provisioning in AWS Singapore Region
3. Using Ubuntu latest Image AMI (Passing through variable)
```
variable "ami" {
  description = "The ID of the Amazon Machine Image (AMI)"
  type        = string
  default     = "ami-0fa377108253bf620"
}
```
4. Crated 2 Public and 2 Private Subnets
```
resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.10.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-public-subnet"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.20.0/24"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-public-subnet"
  }
}

resource "aws_subnet" "private_subnet1" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.100.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-private-subnet"
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.200.0/24"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-private-subnet"
  }
}
```
5. EC2 instances launched in private subnet and AWS ALB launched in public subnet
6. Created CI/Cd pipeline code with two stages (test and terraform-deploy)
```
stages:
  - test
  - terraform-deploy

```
7. Current deployment into target AWS environment only from main branch and with manual approvals

```
terraform-apply:
  stage: terraform-deploy
  script:
    - docker run
      -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
      -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
      -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
      -v $PWD:/terraform
      -w /terraform
      hashicorp/terraform:1.5 init
    - docker run
      -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
      -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
      -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
      -v $PWD:/terraform
      -w /terraform
      hashicorp/terraform:1.5 apply --auto-approve
  when: manual
  only:
    - main

```
8. Terraform statefile is keeping locally.

## CICD

![CICD1](cicd/CICD_1.png)
![CICD2](cicd/CICD_2.png)
![CICD3](cicd/CICD_3.png)
![CICD4](cicd/CICD_4.png)
![CICD5](cicd/CICD_5.png)
