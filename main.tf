resource "aws_vpc" "govtrch_gcc_vpc" {
  cidr_block = "${data.external.restapidata.result["ip_address"]}${data.external.restapidata.result["subnet_size"]}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "govtech-gcc-vpc"
  }
}

resource "aws_internet_gateway" "govtrch_gcc_igw" {
  vpc_id = aws_vpc.govtrch_gcc_vpc.id
}

resource "aws_eip" "nat_gateway" {
  domain   = "vpc"
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id = aws_subnet.public_subnet_1.id
}

resource "aws_route_table" "instance" {
  vpc_id = aws_vpc.govtrch_gcc_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
}

resource "aws_route_table_association" "private1" {
  subnet_id = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.instance.id
}

resource "aws_route_table_association" "private2" {
  subnet_id = aws_subnet.private_subnet2.id
  route_table_id = aws_route_table.instance.id
}

resource "aws_route" "govtrch_gcc_route_table" {
  route_table_id         = aws_vpc.govtrch_gcc_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.govtrch_gcc_igw.id
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.10.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-public-subnet"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.20.0/24"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-public-subnet"
  }
}

resource "aws_subnet" "private_subnet1" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.100.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-private-subnet"
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id                  = aws_vpc.govtrch_gcc_vpc.id
  cidr_block              = "192.168.200.0/24"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "govtech-gcc-private-subnet"
  }
}

data "aws_subnet" "subnet1" {
  depends_on = [aws_subnet.private_subnet1]
  id         = aws_subnet.private_subnet1.id
}

data "aws_subnet" "subnet2" {
  depends_on = [aws_subnet.private_subnet2]
  id         = aws_subnet.private_subnet2.id
}

locals {
  subnet1_id = data.aws_subnet.subnet1.id
  subnet2_id = data.aws_subnet.subnet2.id
}


resource "aws_security_group" "http_sg_external" {
  name        = "govtech-gcc-sg-external"
  description = "Allow inbound traffic on port 80 and 22"
  vpc_id      = aws_vpc.govtrch_gcc_vpc.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "http_sg_internal" {
  name        = "govtech-gcc-sg-internal"
  description = "Allow inbound traffic on port 80 and 22"
  vpc_id      = aws_vpc.govtrch_gcc_vpc.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["${data.external.restapidata.result["ip_address"]}${data.external.restapidata.result["subnet_size"]}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.external.restapidata.result["ip_address"]}${data.external.restapidata.result["subnet_size"]}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
